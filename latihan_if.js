// num < 5 - return "Tiny"
// num < 10 - return "Small"
// num < 15 - return "Medium"
// num < 20 - return "Large"
// num >= 20 - return "Huge"

const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("Input the Number :     ", (numberInput) => {
  grade(numberInput);
  rl.close();
});

function grade(score) {
  if (score < 5) {
    return show("Tiny");
  } else if (score < 10) {
    return show("Small");
  } else if (score < 15) {
    return show("Medium");
  } else if (score < 20) {
    return show("Large");
  } else if (score >= 20) {
    return show("Huge");
  } else {
    return show("Undefined");
  }
}

function show(print) {
  return console.log(`==== ${print} ====`);
}

rl.on("close", () => {
  process.exit();
});
